import React from 'react';
import { Metadata } from 'next';
import { axiosClient } from '@/libraries/axiosClient';
import { AxiosResponse } from 'axios';

type Props = {};

export const metadata: Metadata = {
  title: 'Next.js',
};

export default async function Categories({}: Props) {
  const categories: any = await axiosClient.get('/online-shop/categories');

  return (
    <div>
      <ul>
        {categories.map((category: any) => (
          <li key={category._id}>{category.name}</li>
        ))}
      </ul>
    </div>
  );
}
