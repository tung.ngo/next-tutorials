import axios from 'axios';
import { SERVER_URL } from '../constants/SETTINGS';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ0dW5nbnRAc29mdGVjaC52biIsImVtYWlsIjoidHVuZ250QHNvZnRlY2gudm4iLCJzdWIiOjEsImFwcGxpY2F0aW9uIjoiT25saW5lIFNob3AgLSBBUEkiLCJyb2xlcyI6W3siaWQiOjEsIm5hbWUiOiJBZG1pbmlzdHJhdG9ycyJ9LHsiaWQiOjIsIm5hbWUiOiJNYW5hZ2VycyJ9XSwiaWF0IjoxNjgzODM0Mzg2LCJleHAiOjE2ODQwOTM1ODZ9.NtlzJhy8DerFZuPS9q8brf4phnj82VB1pZSke_dQV90';

const axiosClient = axios.create({
  baseURL: SERVER_URL,
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
  },
});

// RESPONSE
axiosClient.interceptors.response.use(async (response) => {
  return response.data;
});

export { axiosClient };
