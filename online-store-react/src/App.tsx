import React, { useState } from 'react';
import './App.css';

function App() {
  const [categories, setCategories] = useState([]);

  React.useEffect(() => {
    fetch('https://strapi.aptech.io/api/categories?populate=imageUrl&sort[0]=sort')
      .then((res) => {
        return res.json();
      })
      .then((json) => {
        setCategories(json.data);
      });
  }, []);

  return (
    <div>
      <ul>
        {categories.map((category: any) => {
          return <li key={category.id}>{category.attributes.name}</li>;
        })}
      </ul>
    </div>
  );
}

export default App;
