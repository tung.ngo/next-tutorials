import React from 'react';
import Image from 'next/image';
import { Roboto } from 'next/font/google';
import styles from './about.module.css';

// If loading a variable font, you don't need to specify the font weight
// This new font system also allows you to conveniently use all Google Fonts with performance and privacy in mind.
// CSS and font files are downloaded at build time and self-hosted with the rest of your static assets.
// No requests are sent to Google by the browser.
const roboto = Roboto({ weight: ['400', '700'], subsets: ['latin'] });

type Props = {};

export default function About({}: Props) {
  return (
    <div>
      <h1 className={styles.heading}>Đại sứ đặc mệnh toàn quyền Đại Hàn Dân Quốc tại Việt Nam thăm, làm việc tại Trường Đại học Công nghệ Thông tin và Truyền thông Việt - Hàn</h1>
      <div style={{ display: 'flex', marginTop: 24 }}>
        <div>
          <Image src='/images/viet-han.jpeg' width={320} height={320} alt='Picture of the author' />
        </div>
        <div style={{ flex: 1, textAlign: 'justify', paddingLeft: 16 }}>
          <p className={roboto.className}>
            Hiệu trưởng - PGS.TS. Huỳnh Công Pháp cùng lãnh đạo Nhà trường vui mừng đón tiếp Đại sứ và Đoàn công tác đã đến thăm và làm việc. Phát biểu tại buổi làm việc, Hiệu trưởng trân trọng cảm ơn Đại sứ quán Hàn Quốc và KOICA Việt Nam đã đồng hành, hỗ trợ VKU trong suốt thời gian qua, đặc biệt là dự án Nâng cao năng lực giáo dục – đào tạo, quản trị và nghiên cứu của Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn giai đoạn 2022-2027 trị giá 7,7 triệu USD từ nguồn vốn ODA không
            hoàn lại của Chính phủ Hàn Quốc thông qua KOICA.
          </p>
        </div>
      </div>
    </div>
  );
}
