import { useRouter } from 'next/router';
import React from 'react';

type Props = {};

// http://localhost:3000/posts/f1/f2/f3

export default function CatchAllRoutes({}: Props) {
  const router = useRouter();
  const { params = [] } = router.query;
  console.log(params);

  return <div>Catch All Routes</div>;
}
