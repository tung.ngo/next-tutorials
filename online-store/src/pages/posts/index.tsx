import { NEXT_REVALIDATE } from '@/constants/SETTINGS';
import { GetStaticProps } from 'next';
import Link from 'next/link';
import React from 'react';

type Props = {
  posts: any[];
};

export default function Posts({ posts }: Props) {
  return (
    <div>
      <ul>
        {posts.map((post: any) => {
          return (
            <li key={post.id}>
              <Link href={`/posts/${post.id}`}>
                {post.id} {post.title}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

// ------------------------------------------------------------------------------------------------
export const getStaticProps: GetStaticProps = async (context) => {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const data = await response.json();

    return {
      props: {
        posts: data,
      },
      revalidate: NEXT_REVALIDATE,
    };
  } catch (error) {
    return {
      notFound: true,
    };
  }
};
