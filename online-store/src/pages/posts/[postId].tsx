import { NEXT_REVALIDATE } from '@/constants/SETTINGS';
import { GetStaticPaths, GetStaticProps } from 'next';
import React from 'react';

type Props = {
  post: any;
};

export default function PostDetails({ post }: Props) {
  return (
    <div>
      <h4>Post Details: {post?.id}</h4>
      <hr />
      <strong> {post?.title}</strong>
      <p>{post?.body}</p>
    </div>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  console.log('GetStaticPaths: Posts');
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await response.json();

  const paths = data.map((item: any) => ({
    params: {
      postId: `${item.id}`,
    },
  }));

  return { paths, fallback: true };
};

export const getStaticProps: GetStaticProps = async (context: any) => {
  try {
    const { postId } = context.params;

    const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + postId);
    const data = await response.json();

    if (!data) {
      return {
        notFound: true,
      };
    }

    return {
      props: {
        post: data,
      },
      revalidate: NEXT_REVALIDATE,
    };
  } catch (error) {
    return {
      notFound: true,
    };
  }
};
