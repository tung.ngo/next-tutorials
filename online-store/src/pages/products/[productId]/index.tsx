import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';
import React from 'react';

import { NEXT_REVALIDATE, SERVER_URL } from '@/constants/SETTINGS';
import { axiosClient } from '@/library/axiosClient';
import { Button, ButtonGroup, Card, CardBody, CardFooter, CardHeader, Container, Divider, Heading, Image, Spinner, Stack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';

type Props = {
  product: any;
};

export default function ProductDetails({ product }: Props) {
  const router = useRouter();
  // Fallback = true
  if (router.isFallback) {
    return (
      <Container>
        <Spinner />
      </Container>
    );
  }

  return (
    <React.Fragment>
      <Head>
        <title>{product?.attributes?.name}</title>
        <meta name='description' content='Product of Online Store' />
      </Head>
      <main>
        <Card direction={{ base: 'column', sm: 'row' }} overflow='hidden' variant='outline' padding={4}>
          <Image objectFit='cover' maxW={{ base: '100%', sm: '400px' }} src={`${SERVER_URL}${product?.attributes?.imageUrl?.data?.attributes?.url}`} alt='' />
          <Stack width='100%'>
            <CardBody>
              <Text as='strong'>{product?.attributes?.category?.data?.attributes?.name}</Text>
              <Heading size='md'>{product?.attributes?.name}</Heading>
              <Divider my={4} />
              <Stack spacing={1}>
                <Text as='strong'>Giá bán: {product?.attributes?.price}</Text>
                <Text as='strong'>Giảm: {product?.attributes?.discount}%</Text>
              </Stack>
            </CardBody>

            <CardFooter>
              <ButtonGroup spacing={4}>
                <Button variant='solid' colorScheme='brand'>
                  Buy now
                </Button>

                <Button variant='ghost' colorScheme='brand'>
                  Add to cart
                </Button>
              </ButtonGroup>
            </CardFooter>
          </Stack>
        </Card>
        <Text py={8}>{product?.attributes?.description}</Text>
      </main>
    </React.Fragment>
  );
}

// ------------------------------------------------------------------------------------------------
// GetStaticPaths
// ------------------------------------------------------------------------------------------------
export const getStaticPaths: GetStaticPaths = async () => {
  console.log('GetStaticPaths: Products');

  const response = await axiosClient.get('/api/products?fields[0]=name&populate=category');
  const products = response.data;

  const paths = products.map((item: any) => ({
    params: {
      productId: `${item.id}`,
    },
  }));

  return { paths, fallback: true };
};

// ------------------------------------------------------------------------------------------------
// GetStaticProps
// ------------------------------------------------------------------------------------------------
interface IParams extends ParsedUrlQuery {
  productId: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { productId } = context.params as IParams;

  try {
    const response = await axiosClient.get(`/api/products/${productId}/?populate=*`);
    const product = response.data;

    return {
      props: {
        product,
      },

      revalidate: NEXT_REVALIDATE,
    };
  } catch (error) {
    return {
      notFound: true,
    };
  }
};
