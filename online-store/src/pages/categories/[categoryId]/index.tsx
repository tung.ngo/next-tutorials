import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';
import NextLink from 'next/link';
import { ParsedUrlQuery } from 'querystring';
import React from 'react';

import { NEXT_REVALIDATE, SERVER_URL } from '@/constants/SETTINGS';
import { axiosClient } from '@/library/axiosClient';
import { Box, Button, ButtonGroup, Card, CardBody, CardFooter, CardHeader, Heading, Image, LinkBox, SimpleGrid, Spacer, Stack, Text, Breadcrumb, BreadcrumbItem, BreadcrumbLink, BreadcrumbSeparator, Divider, Container, Spinner } from '@chakra-ui/react';
import _ from 'lodash';
import { useRouter } from 'next/router';

export default function Products({ products = [] }) {
  let firstProduct = _.first(products) as any;
  let categoryName = '';
  categoryName = firstProduct?.attributes?.category?.data?.attributes?.name;

  const router = useRouter();
  // Fallback = true
  if (router.isFallback) {
    return (
      <Container>
        <Spinner />
      </Container>
    );
  }

  return (
    <React.Fragment>
      <Head>
        <title>Products</title>
        <meta name='description' content='Categories of Online Store' />
      </Head>
      <main>
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink as={NextLink} href='/categories'>
              Danh mục
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <BreadcrumbLink as={NextLink} href='/categories/'>
              {categoryName}
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        <Divider my={6} />
        <SimpleGrid column={[2, 3, 4]} minChildWidth='300px' columnGap={8} rowGap={12}>
          {products &&
            products.map((product: any) => {
              return (
                <Box key={product.id} maxWidth='300px'>
                  <LinkBox as={NextLink} href={`/products/${product.id}`}>
                    <Card>
                      <CardBody>
                        <Image src={`${SERVER_URL}${product.attributes?.imageUrl?.data?.attributes?.url}`} alt='Green double couch with wooden legs' borderRadius='lg' />
                        <Stack mt='6' spacing='3'>
                          <Heading size='md'>{product.attributes.name}</Heading>
                          {/* PRICE */}
                          <Box display='flex'>
                            <Text as='strong'>Giá bán:</Text>
                            <Spacer />
                            <Text>{product.attributes.price}</Text>
                          </Box>
                          {/* DISCOUNT */}
                          <Box display='flex'>
                            <Text as='strong'>Giảm:</Text>
                            <Spacer />
                            <Text>{product.attributes.discount}%</Text>
                          </Box>
                        </Stack>
                      </CardBody>
                      <CardFooter>
                        <ButtonGroup spacing={4}>
                          <Button variant='solid' colorScheme='brand'>
                            Buy now
                          </Button>

                          <Button variant='ghost' colorScheme='brand'>
                            Add to cart
                          </Button>
                        </ButtonGroup>
                      </CardFooter>
                    </Card>
                  </LinkBox>
                </Box>
              );
            })}
        </SimpleGrid>
      </main>
    </React.Fragment>
  );
}

// ------------------------------------------------------------------------------------------------
// GetStaticPaths
// ------------------------------------------------------------------------------------------------
export const getStaticPaths: GetStaticPaths = async () => {
  console.log('GetStaticPaths: Categories', new Date());

  const response = await axiosClient.get('/api/categories?fields[0]=name');
  const categories = response.data;

  const paths = categories.map((item: any) => ({
    params: { categoryId: `${item.id}` },
  }));

  return { paths, fallback: true };
};

// ------------------------------------------------------------------------------------------------
// GetStaticProps
// ------------------------------------------------------------------------------------------------
interface IParams extends ParsedUrlQuery {
  categoryId: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { categoryId } = context.params as IParams;

  try {
    const response = await axiosClient.get(`/api/products?filters[category][id][$eq]=${categoryId}&sort[0]=price&populate=*`);
    const products = response.data;
    return {
      props: {
        products,
      },

      revalidate: NEXT_REVALIDATE,
    };
  } catch (error) {
    return {
      notFound: true,
    };
  }
};
