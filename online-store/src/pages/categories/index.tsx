import Head from 'next/head';
import { Card, CardBody, Heading, Text, Stack, Image, SimpleGrid, Box, LinkBox } from '@chakra-ui/react';
import { GetStaticProps, GetStaticPropsContext } from 'next';
import { axiosClient } from '@/library/axiosClient';
import { NEXT_REVALIDATE, SERVER_URL } from '@/constants/SETTINGS';
import NextLink from 'next/link';
import React from 'react';

export default function Categories({ categories = [] }) {
  return (
    <React.Fragment>
      <Head>
        <title>Categories</title>
        <meta name='description' content='Categories of Online Store' />
      </Head>
      <main>
        <Heading as='h2' size='md'>
          Danh mục sản phẩm
        </Heading>
        <Box minHeight={12} marginTop={2}></Box>
        <SimpleGrid column={[2, 3, 4]} minChildWidth='300px' columnGap={8} rowGap={12}>
          {categories &&
            categories.map((category: any) => {
              return (
                <Box key={category.id}>
                  <LinkBox as={NextLink} href={`/categories/${category.id}`}>
                    <Card>
                      <CardBody>
                        <Image src={`${SERVER_URL}${category.attributes?.imageUrl?.data?.attributes?.url}`} alt='Green double couch with wooden legs' borderRadius='lg' />
                        <Stack mt='6' spacing='3'>
                          <Heading size='md'>{category.attributes.name}</Heading>
                          <Text>{category.attributes.description}</Text>
                        </Stack>
                      </CardBody>
                    </Card>
                  </LinkBox>
                </Box>
              );
            })}
        </SimpleGrid>
      </main>
    </React.Fragment>
  );
}

// ------------------------------------------------------------------------------------------------
// GetStaticProps
// ------------------------------------------------------------------------------------------------
export const getStaticProps: GetStaticProps = async (context: GetStaticPropsContext) => {
  const response = await axiosClient.get('/api/categories?populate=imageUrl&sort[0]=sort');
  const categories = response.data;

  return {
    props: {
      categories,
    },
    revalidate: NEXT_REVALIDATE,
  };
};
