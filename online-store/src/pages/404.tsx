import { Box, Center } from '@chakra-ui/react';
import React from 'react';

type Props = {};

export default function Page404({}: Props) {
  return (
    <Box>
      <Center textAlign='center'>404 | Not Found</Center>
    </Box>
  );
}
