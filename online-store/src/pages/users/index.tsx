import { GetServerSideProps } from 'next';
import Link from 'next/link';
import React from 'react';

type Props = {
  users: any[];
  time: string;
};

export default function Users({ users, time }: Props) {
  return (
    <div>
      <h2>Time: {time}</h2>
      <ul>
        {users.map((user: any) => {
          return (
            <li key={user.id}>
              <Link href={`/users/${user.id}`}>
                {user.id} {user.username} {user.email}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

// ------------------------------------------------------------------------------------------------
export const getServerSideProps: GetServerSideProps = async (context) => {
  const response = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await response.json();

  return {
    props: {
      users: data,
      time: new Date().toISOString(),
    },
  };
};
