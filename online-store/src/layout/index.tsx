import { axiosClient } from '@/library/axiosClient';
import { Box, Link, Flex, SimpleGrid, Divider, Container } from '@chakra-ui/react';
import NextLink from 'next/link';

import React, { useState } from 'react';

type Props = {
  children: React.ReactNode;
};

export default function Layout({ children }: Props) {
  const [categories, setcategories] = useState([]);

  // GET DATA (CLIENT SIDE)
  React.useEffect(() => {
    const getCategories = async () => {
      const response = await axiosClient.get('/api/categories');
      setcategories(response.data);
    };

    getCategories();
  }, []);

  return (
    <Container maxW='container.xl'>
      <Box mx='auto' p={{ base: '20px' }} minHeight='100vh'>
        <Flex gap={12} flexWrap='wrap'>
          <Box minWidth={160} display='flex' flexDirection='column'>
            <Link as={NextLink} href='/'>
              <strong>Trang chủ</strong>
            </Link>
            <Divider marginBlock={2} />
            <Link as={NextLink} href='/about'>
              <strong>Giới thiệu</strong>
            </Link>
            <Divider marginBlock={2} />
            <Link as={NextLink} href='/categories'>
              <strong>Danh mục</strong>
            </Link>
            <Divider marginBlock={2} />

            {categories &&
              categories.map((category: any) => {
                return (
                  <div key={category.id}>
                    <Link as={NextLink} href={`/categories/${category.id}`}>
                      <strong>{category.attributes.name}</strong>
                    </Link>
                  </div>
                );
              })}
          </Box>

          <Box flex={4}>{children}</Box>
        </Flex>
      </Box>
    </Container>
  );
}
