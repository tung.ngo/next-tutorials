import axios from 'axios';
import { SERVER_URL } from '../constants/SETTINGS';

const axiosClient = axios.create({
  baseURL: SERVER_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

// RESPONSE
axiosClient.interceptors.response.use(async (response) => {
  return response.data;
});

export { axiosClient };
